package fr.ubs;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ContactServiceMockTest {
    
    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Test
    void shouldFailDuplicate(){
        Mockito.when(contactDao.isContactExist("ewan")).thenReturn(true);
        assertThrows(IllegalArgumentException.class, () -> contactService.creerContact("ewan"));
    }

    @Test
    void shouldPass(){
        Mockito.when(contactDao.isContactExist("ewan")).thenReturn(false);

        contactService.creerContact("ewan");
    }
}

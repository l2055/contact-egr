package fr.ubs;

public class ContactNotFoundException extends RuntimeException {

    public ContactNotFoundException() {
        super("Le contact n'existe pas");
    }

}

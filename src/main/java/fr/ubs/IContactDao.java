package fr.ubs;

public interface IContactDao {
    boolean isContactExist(String nom);
    void addContact(String nom);
    void delete(String nom);
}
